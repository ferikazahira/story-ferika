from django.apps import AppConfig


class PersonalwebsiteConfig(AppConfig):
    name = 'personalwebsite'
