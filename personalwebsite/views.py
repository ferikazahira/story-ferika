from django.shortcuts import render

# Create your views here.

def welcome(request):
    return render(request, 'personalwebsite/welcome.html')

def index(request):
    return render(request, 'personalwebsite/index.html')

def newpage(request):
    return render(request, 'personalwebsite/newpage.html')