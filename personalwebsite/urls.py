from django.urls import path
from . import views

app_name = 'personalwebsite'

urlpatterns = [
    path('', views.welcome, name='welcome'),
    path('profil/', views.index, name='index'),
    path('education/', views.newpage, name='education')
]