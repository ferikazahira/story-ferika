# Generated by Django 3.1.1 on 2020-10-16 17:37

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PostModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama_matkul', models.CharField(max_length=100)),
                ('nama_dosen', models.CharField(max_length=100)),
                ('jumlah_sks', models.IntegerField()),
                ('semester', models.CharField(max_length=100)),
                ('ruang_kelas', models.CharField(max_length=100)),
                ('deskripsi', models.CharField(max_length=100, null=True)),
            ],
        ),
    ]
