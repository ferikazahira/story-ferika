from django.urls import path
from . import views

app_name = 'storylima'

urlpatterns = [
    path('storylima',views.listMatkul,name='listMatkul'),
    path('TambahkanMatkul',views.add, name='add'),
    path('delete/P<int:delete_id>/',views.delete, name='delete'),
]

