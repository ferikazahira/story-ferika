from django.db import models

# Create your models here.

class PostModel(models.Model):
    nama_matkul = models.CharField(max_length = 100)
    nama_dosen = models.CharField(max_length=100)
    jumlah_sks = models.IntegerField()
    semester = models.CharField(max_length = 100)
    ruang_kelas = models.CharField(max_length = 100)
    deskripsi = models.CharField(max_length = 100, null = True)
    
    def __str__(self):
        return "{}.{}".format(self.id, self.nama_matkul)