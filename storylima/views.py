from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import MatkulForm
from .models import PostModel
# Create your views here.

def delete(request,delete_id):
	PostModel.objects.filter(id=delete_id).delete()
	return redirect('storylima:listMatkul')
    
def listMatkul(request):
    posts = PostModel.objects.all()
    context = {
        'page_title':'List Mata Kuliah',
        'posts':posts,
    }
    return render(request, 'storylima/listMatkul.html',context)

def add(request):
    matkul_form = MatkulForm(request.POST or None)

    if matkul_form.is_valid():
        if request.method == 'POST':
            PostModel.objects.create(
                nama_matkul = matkul_form.cleaned_data.get('nama_matkul'),
                nama_dosen = matkul_form.cleaned_data.get('nama_dosen'),
                jumlah_sks = matkul_form.cleaned_data.get('jumlah_sks'),
                semester = matkul_form.cleaned_data.get('semester'),
                ruang_kelas = matkul_form.cleaned_data.get('ruang_kelas'),
                deskripsi = matkul_form.cleaned_data.get('deskripsi'),
                )
            return HttpResponseRedirect("/storylima/storylima")

            

    context = {
        'page_title':'Tambahkan Mata Kuliah',
        'matkul_form':matkul_form,
    }
    return render(request, 'storylima/add.html', context)
    