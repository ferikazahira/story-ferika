from django.shortcuts import render,redirect
from .forms import FormKegiatan, FormDaftar
from .models import Kegiatan, Daftar
# Create your views here.

def pilih(request):
    return render(request, 'storyenam/pilih.html')

def addKegiatan(request):
    form_kegiatan = FormKegiatan(request.POST or None)
    if request.method == "POST":
        if form_kegiatan.is_valid():
            Kegiatan.objects.create(
                nama_kegiatan = form_kegiatan.cleaned_data.get('nama_kegiatan'),
                deskripsi = form_kegiatan.cleaned_data.get('deskripsi'),
                )
            return redirect('/story6/')
    
    context = {
        'form_kegiatan': form_kegiatan,
    }
    return render(request, 'storyenam/addKegiatan.html', context)

def listKegiatan(request):
    kegiatan = Kegiatan.objects.all()
    daftar = Daftar.objects.all()
    context= {
        'kegiatan': kegiatan,
        'daftar': daftar,
    }
    return render(request, 'storyenam/listKegiatan.html', context)

def deleteUser(request, delete_id):
    kegiatan = Kegiatan.objects.all()
    daftar = Daftar.objects.all()
    print(delete_id)
    daftar_to_delete = Daftar.objects.get(id=delete_id)
    daftar_to_delete.delete()
    return redirect('/story6/listKegiatan')


def daftar(request, task_id):
    form_nama = FormDaftar()

    if request.method == "POST":
        form_nama_input = FormDaftar(request.POST)
        if form_nama_input.is_valid():
            data = form_nama_input.cleaned_data
            orangBaru = Daftar()
            orangBaru.nama = data['nama']
            orangBaru.kegiatan = Kegiatan.objects.get(id=task_id)
            orangBaru.save()
            return redirect('/story6/listKegiatan')
    context={
        'form_nama': form_nama,
    }
    return render(request, 'storyenam/daftar.html', context)


