from django.contrib import admin
from django.urls import path, include
from .views import pilih, addKegiatan, listKegiatan, daftar, deleteUser

app_name = 'storyenam'

urlpatterns = [
    path('', pilih, name='pilih'),
    path('addKegiatan/', addKegiatan, name='addKegiatan'),
    path('listKegiatan/', listKegiatan, name='listKegiatan'),
    path('daftar/<int:task_id>/', daftar, name='daftar'),
    path('delete/<int:delete_id>/', deleteUser, name='delete'),
]