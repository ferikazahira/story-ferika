from django.contrib import admin
from .models import Kegiatan, Daftar

# Register your models here.
admin.site.register(Kegiatan)
admin.site.register(Daftar)
