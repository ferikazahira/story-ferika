from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length=100)
    deskripsi = models.CharField(max_length=1000)

    def __str__(self):
        return self.nama_kegiatan


class Daftar(models.Model):
    nama = models.CharField(max_length=100)
    kegiatan = models.ForeignKey(Kegiatan,
                                 on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.nama
