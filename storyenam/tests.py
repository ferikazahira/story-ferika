from django.test import TestCase
from django.test import Client 
from django.urls import resolve, reverse
from .models import Kegiatan, Daftar
from .views import pilih, addKegiatan, listKegiatan, deleteUser, daftar
from .forms import FormKegiatan, FormDaftar
from django.apps import apps
from .apps import StoryenamConfig
# Create your tests here.

class ModelTest(TestCase):
    def setUp(self):
        self.kegiatan = Kegiatan.objects.create(
            nama_kegiatan="belajar", deskripsi ="belajar bareng")
        self.daftar = Daftar.objects.create(nama="Ferika")

    def test_model_dibuat(self):
        self.assertEqual(Kegiatan.objects.count(), 1)
        self.assertEqual(Daftar.objects.count(), 1)

    def test_str(self):
        self.assertEquals(str(self.kegiatan), "belajar")
        self.assertEquals(str(self.daftar), "Ferika")

class FormTest(TestCase):
    def test_form_is_valid(self):
        form_kegiatan = FormKegiatan(data={
            "nama_kegiatan": "belajar",
            "deskripsi": "belajar bareng"
        })
        self.assertTrue(form_kegiatan.is_valid())

        form_daftar = FormDaftar(data={
            'nama': 'Ferika'
        })
        self.assertTrue(form_daftar.is_valid())
    
    def test_form_invalid(self):
        form_kegiatan = FormKegiatan(data={})
        self.assertFalse(form_kegiatan.is_valid())
        form_daftar = FormDaftar(data={})
        self.assertFalse(form_daftar.is_valid())

class UrlsTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_url_pilih_ada(self):
        response = Client().get('/story6')
        self.assertEquals(response.status_code, 301)
    
    def test_url_addKegiatan_ada(self):
        response = Client().get('/story6/addKegiatan')
        self.assertEquals(response.status_code, 301)
    
    def test_url_listKegiatan_ada(self):
        response = Client().get('/story6/listKegiatan')
        self.assertEquals(response.status_code, 301)
    
    def test_url_daftar_ada(self):
        response = Client().get('/story6/daftar/1')
        self.assertEquals(response.status_code, 301)
    
    def test_delete_use_right_function(self):
        response = Client().get('/story6/delete/1')
        self.assertEquals(response.status_code, 301)
 
class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.pilih = reverse("storyenam:pilih")
        self.listKegiatan = reverse("storyenam:listKegiatan")
        self.addKegiatan = reverse("storyenam:addKegiatan")

    def test_GET_index(self):
        response = self.client.get(self.pilih)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'storyenam/pilih.html')

    def test_GET_listKegiatan(self):
        response = self.client.get(self.listKegiatan)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'storyenam/listKegiatan.html')

    def test_GET_addKegiatan(self):
        response = self.client.get(self.addKegiatan)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'storyenam/addKegiatan.html')

    def test_POST_addKegiatan(self):
        response = self.client.post(self.addKegiatan,{'nama': 'liburan', 'deskripsi': "belajar"}, follow = True)
        self.assertEqual(response.status_code, 200)

    def test_POST_addKegiatan_invalid(self):
        response = self.client.post(self.addKegiatan,{'nama': '', 'deskripsi': ""}, follow = True)
        self.assertTemplateUsed(response, 'storyenam/addKegiatan.html')

    def test_GET_delete(self):
        kegiatan = Kegiatan(nama_kegiatan="abc", deskripsi="CDF")
        kegiatan.save()
        daftar = Daftar(nama="mangoleh",
                      kegiatan=Kegiatan.objects.get(pk=1))
        daftar.save()
        response = self.client.get(reverse('storyenam:delete', args=[daftar.pk]))
        self.assertEqual(Daftar.objects.count(), 0)
        self.assertEqual(response.status_code, 302)

class TestRegist(TestCase):
    def setUp(self):
        kegiatan = Kegiatan(nama_kegiatan="abc", deskripsi="CDF")
        kegiatan.save()

    def test_regist_POST(self):
        response = Client().post('/story6/daftar/1/',
                                 data={'nama': 'bangjago'})
        self.assertEqual(response.status_code, 302)

    def test_regist_GET(self):
        response = self.client.get('/story6/daftar/1/')
        self.assertTemplateUsed(response, 'storyenam/daftar.html')
        self.assertEqual(response.status_code, 200)

    def test_regist_POST_invalid(self):
        response = Client().post('/story6/daftar/1/',
                                 data={'nama': ''})
        self.assertTemplateUsed(response, 'storyenam/daftar.html')
        self.assertEqual(response.status_code, 200)

class TestHtml(TestCase):
    def setUp(self):
        self.client = Client()

    def test_html_pilih_sesuai(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'storyenam/pilih.html')
    
    def test_html_pilih_sesuai(self):
        response = Client().get('/story6/addKegiatan/')
        self.assertTemplateUsed(response, 'storyenam/addKegiatan.html')
    
    def test_html_pilih_sesuai(self):
        response = Client().get('/story6/listKegiatan/')
        self.assertTemplateUsed(response, 'storyenam/listKegiatan.html')

    def test_html_pilih_sesuai(self):
        response = Client().get('/story6/daftar/1/')
        self.assertTemplateUsed(response, 'storyenam/daftar.html')
    
class TestApps(TestCase):
    def test_apps(self):
        self.assertEqual(StoryenamConfig.name, 'storyenam') 
        self.assertEqual(apps.get_app_config('storyenam').name, 'storyenam')